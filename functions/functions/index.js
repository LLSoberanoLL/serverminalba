const express = require('express');
const cors = require('cors');

const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp();

const app = express();

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));

app.get('/user_id', (req, res) => {

    var params = req.params;

    admin
        .auth()
        .getUserByEmail(params.email)
        .then((userRecord) => {
            // See the UserRecord reference doc for the contents of userRecord.
            res.send(userRecord.uid);
        })
        .catch((error) => {
            console.log('Error fetching user data:', error);
        });
})

const database = admin.database();

const getDayOfWeekFromDate = function (date) {
    switch (date.getDay()) {
        case 0:
            return "sunday";
        case 1:
            return "monday";
        case 2:
            return "tuesday";
        case 3:
            return "wednesday";
        case 4:
            return "thursday";
        case 5:
            return "friday";
        case 6:
            return "saturday";
    }
}

// exports.resetPDVIdeal = functions.pubsub.schedule("0 0 * * 0").timeZone("America/Sao_Paulo").onRun(async context => {

//     const snapshots = await database.ref(`pdv_list`).once("value");
//     var pdvs = []
//     snapshots.forEach(snapshot => {
//         pdvs.push(snapshot.key);
//     });

//     await Promise.all(pdvs.map(async pdv_id => {
//         await database.ref(`pdv_list/${pdv_id}`).update({
//             pdv_status: 0,
//             last_points: 0,
//             last_reviews: "",
//             last_reviews_user_id: "",
//             updated_at: new Date().getTime()
//         });
//     }));

//     return console.log(`PDVs ideal reseted at: ${new Date().toLocaleDateString()}`);
// });

exports.createVisits = functions.pubsub.schedule("0 0 * * *").timeZone("America/Sao_Paulo").onRun(async context => {

    var now = new Date();
    var pdvs = await database.ref("pdv_list").once("value");
    var pdv_list = {};
    var script_list = [];
    pdvs.forEach(snapshot => {
        var pdv = snapshot.val();
        pdv_list[snapshot.key] = pdv;
    });
    var scripts = await database.ref(`scripts`).once("value");
    scripts.forEach(snapshot => {
        var script = snapshot.val();
        script_list.push(script);
    });
    var year = now.getFullYear();
    var month = now.getMonth() + 1 < 10 ? "0" + (now.getMonth() + 1) : now.getMonth() + 1;
    var day = now.getDate() < 10 ? "0" + now.getDate() : now.getDate();
    await Promise.all(script_list.map(async script => {
        for (var day_of_week in script.script_list) {
            if (day_of_week == getDayOfWeekFromDate(now)) {
                
                var pdvs_from_day = script.script_list[day_of_week];
                
                for(var pdv_id in pdvs_from_day) {
                    
                    if(script.user_id) {
                       
                        const visit = await database.ref(`visits/${pdv_id}/${year}/${month}/${day}`).orderByChild("user_id").equalTo(script.user_id).once("value");
                        
                        if(!visit.exists()) {

                            var visit_push = await database.ref(`visits/${pdv_id}/${year}/${month}/${day}`).push();

                            await database.ref(`visits/${pdv_id}/${year}/${month}/${day}/${visit_push.key}`).update({
                                user_id: script.user_id,
                                scheduled: true,
                                created_at: now.getTime(),
                                updated_at: now.getTime(),
                            })

                            await database.ref(`index_visits_days/${visit_push.key}`).update({
                                visit_ref: `${pdv_id}/${year}/${month}/${day}/${visit_push.key}`,
                                
                                date_filter: `${day}/${month}/${year}/${pdv_id}`,
                                date_filter_user: `${day}/${month}/${year}/${script.user_id}`,

                                year_filter: `${year}/${pdv_id}`,
                                month_filter: `${month}/${pdv_id}`,
                                day_filter: `${day}/${pdv_id}`,
                                
                                year_filter_user: `${year}/${script.user_id}`,
                                month_filter_user: `${month}/${script.user_id}`,
                                day_filter_user: `${day}/${script.user_id}`,
                                
                                user_id: script.user_id,
                                pdv_id: pdv_id,
                                date_stamp: `${day}/${month}/${year}`,
                                visit_id: visit_push.key,
                                
                                created_at: now.getTime(),
                                updated_at: now.getTime(),
                            })

                        } else {

                            var visit_id = Object.keys(visit.val())[0];

                            await database.ref(`visits/${pdv_id}/${year}/${month}/${day}/${visit_id}`).update({
                                user_id: script.user_id,
                                scheduled: true,
                                updated_at: now.getTime(),
                            })

                            await database.ref(`index_visits_days/${visit_id}`).update({
                                visit_ref: `${pdv_id}/${year}/${month}/${day}/${visit_id}`,
                                date_filter: `${day}/${month}/${year}/${pdv_id}`,
                                date_filter_user: `${day}/${month}/${year}/${script.user_id}`,

                                year_filter: `${year}/${pdv_id}`,
                                month_filter: `${month}/${pdv_id}`,
                                day_filter: `${day}/${pdv_id}`,
                                
                                year_filter_user: `${year}/${script.user_id}`,
                                month_filter_user: `${month}/${script.user_id}`,
                                day_filter_user: `${day}/${script.user_id}`,

                                user_id: script.user_id,
                                pdv_id: pdv_id,
                                date_stamp: `${day}/${month}/${year}`,
                                visit_id: visit_id
                               
                            })
                        }
                    }

                }

            }
        }
    }));

    return console.log(`Visits created at: ${day}/${month}/${year}`);
});

exports.uid = functions.https.onRequest(app)